const {
  createAppSpec,
} = require('../lib/appSpec');

describe('Entities are defined', () => {
  let appSpec;

  before(() => createAppSpec().then((spec) => { appSpec = spec; }));

  [
    ['user', 'users'],
    ['team', 'teams'],
    ['board', 'boards'],
    ['list', 'lists'],
    ['card', 'cards'],
  ].forEach(
    ([singular, plural]) =>
      specify(`${plural}(${singular}) should be defined`, () =>
        appSpec.describeEntity(singular).should.include({
          kind: 'entity',
          type: singular,
          pluralType: plural,
        })
      )
  );
});

describe('Traits are defined', () => {
  let appSpec;

  before(() => createAppSpec().then((spec) => { appSpec = spec; }));

  specify('the editable trait has a title, summary and description', () =>
      appSpec.describeTrait('editable').should.deep.include({
        kind: 'trait',
        type: 'editable',
        fields: {
          title: {
            kind: 'field',
            type: 'text',
            name: 'title',
          },
          summary: {
            kind: 'field',
            type: 'text',
            name: 'summary',
          },
          description: {
            kind: 'field',
            type: 'text',
            name: 'description',
          },
        },
      })
  );
});
