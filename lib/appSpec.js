const path = require('path');
const fs = require('fs');

const appSpec = ({
  entities,
  traits,
}) => ({
  describeEntity: type => entities[type],
  describeTrait: what => traits[what],
});

const extractList = string =>
  string
  .trim()
  .replace(/\band\b|,|\ba\b/g, ' ')
  .split(/\s+/g)
  .filter(item => item.length > 0)
;

const parseRules = (rules) => {
  const entities = {};
  const traits = {};

  for (const rawRule of rules) {
    const rule = rawRule.toLowerCase().trim();

    const entityDef = /^there\s+are\s+(.*)/.exec(rule);
    if (entityDef) {
      extractList(entityDef[1])
      .forEach(
        (entity) => {
          const singular = entity.replace(/s$/, '');

          entities[singular] = {
            kind: 'entity',
            type: singular,
            pluralType: entity,
          };
        }
      );
    }

    // an editable has a title, a summary, and a description
    const fieldsTrait = /^an?\s+(\w+)\s+has\s+(.*)/.exec(rule);
    if (fieldsTrait) {
      const type = fieldsTrait[1];
      const fields = {};
      extractList(fieldsTrait[2]).forEach(
        (fieldName) => {
          fields[fieldName] = {
            kind: 'field',
            type: 'text',
            name: fieldName,
          };
        }
      );
      traits[type] = {
        kind: 'trait',
        type: 'editable',
        fields,
      };
    }
  }

  return { entities, traits };
};

const createAppSpec = () => new Promise(
  (resolve, reject) =>
    fs.readFile(
      path.resolve(__dirname, '..', 'app-specification', 'rules.txt'),
      (err, data) => {
        if (err) {
          reject(err);
        } else {
          resolve(
            appSpec(parseRules(data.toString().split('\n')))
          );
        }
      }
    )
);

module.exports = {
  createAppSpec,
};
